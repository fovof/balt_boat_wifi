from django.contrib import admin

# Register your models here.
from .models import RUser, RUserInfo, Subdivision
class RUserInfoInline(admin.TabularInline):
	model = RUserInfo

@admin.register(RUser)
class RUserAdmin(admin.ModelAdmin):
	list_display = ('username', 'password')
	list_filter = ('username', 'password')
	inlines = [RUserInfoInline]

@admin.register(RUserInfo)
class RUserInfoAdmin(admin.ModelAdmin):
	list_display = ('user', 'name', 'surname', 'end_of_access')
	list_filter = ('user', 'end_of_access')
admin.site.register(Subdivision)