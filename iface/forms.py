from django import forms
from .models import RUser, RUserInfo, Subdivision
import random
from django.forms import inlineformset_factory

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
import datetime #for checking renewal date range.


class RUserForm(forms.ModelForm):
    class Meta:
        model = RUser
        fields = ["username", "password"]

class RUserInfoForm(forms.ModelForm):
    class Meta:
        model = RUserInfo
        fields = ['name', 'surname', 'patronymic', 'end_of_access']







