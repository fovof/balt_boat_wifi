from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.base_user import BaseUserManager
from .base.models import (
    AbstractNas, AbstractRadiusAccounting, AbstractRadiusCheck, AbstractRadiusGroup,
    AbstractRadiusGroupCheck, AbstractRadiusGroupReply, AbstractRadiusGroupUsers, AbstractRadiusPostAuth,
    AbstractRadiusReply, AbstractRadiusUserGroup,
)
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.validators import ASCIIUsernameValidator, UnicodeUsernameValidator
from django.utils import six, timezone
from swapper import swappable_setting
from django.urls import reverse 
# Create your models here.

class RUser(models.Model):
    username_validator = UnicodeUsernameValidator() if six.PY3 else ASCIIUsernameValidator()
    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    password = models.CharField(_('password'), max_length=128);
    def __str__(self):
        return self.username

    def get_absolute_url(self):
        """
        Returns the url to access a particular book instance.
        """
        return reverse('ruser', args=[str(self.id)])


class RUserInfo(models.Model):
    user = models.OneToOneField('RUser', on_delete=models.CASCADE, default=None, null=True, blank=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    surname = models.CharField(max_length=200, null=True, blank=True)
    patronymic = models.CharField(max_length=200, null=True, blank=True)
    end_of_access = models.DateField(null=True, blank=True)

    #REQUIRED_FIELDS = ['user']
    class Meta:
        ordering = ["end_of_access"]
        permissions = (("can_see_users", "See and edit users"), ("costil", "See and edit users"))
        

    def __str__(self):
        """
        String for representing the Model object
        """
        if self.user != None:
        	return '%s (%s)' % (self.id,self.user.username)
        else:
        	return '%s' % (self.id)


class RadiusAccounting(AbstractRadiusAccounting):
    class Meta(AbstractRadiusAccounting.Meta):
        abstract = False
        swappable = swappable_setting('iface', 'RadiusAccounting')

class RadiusPostAuth(AbstractRadiusPostAuth):
    class Meta(AbstractRadiusPostAuth.Meta):
        abstract = False
        swappable = swappable_setting('iface', 'RadiusPostAuth')
