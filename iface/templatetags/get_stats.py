from django import template
from subprocess import Popen, PIPE, call
from django.shortcuts import render

register = template.Library()

@register.simple_tag(name="radversion")
def radversion():
	p = Popen('freeradius -v', shell=True, stdout=PIPE)
	out, err = p.communicate()
	return bytes.decode(out)

@register.simple_tag(name="radstatus1")
def radstatus1():
	p = Popen('echo "Message-Authenticator = 0x00, FreeRADIUS-Statistics-Type = 1, Response-Packet-Type = Access-Accept" | radclient -x localhost:18121 status adminsecret', shell=True, stdout=PIPE)
	out, err = p.communicate()
	return bytes.decode(out).split('\n')

@register.simple_tag(name="radstatus2")
def radstatus2():
	p = Popen('echo "Message-Authenticator = 0x00, FreeRADIUS-Statistics-Type = 2, Response-Packet-Type = Access-Accept" | radclient -x localhost:18121 status adminsecret', shell=True, stdout=PIPE)
	out, err = p.communicate()
	return bytes.decode(out).split('\n')
