from django.conf.urls import url

from . import views
from django.conf.urls import include

urlpatterns = [
    url(r'^$', views.MainIndexView.as_view(), name='index'),
    url(r'^ruser/$', views.IndexView.as_view(), name='user_list'), 
    url(r'^sort=name/$', views.IndexNameSortView.as_view(), name='indexns'),
    url(r'^sort=-name/$', views.IndexNameSortReverseView.as_view(), name='indexnsr'),
    url(r'^sort=user/$', views.IndexUserSortView.as_view(), name='indexus'),
    url(r'^sort=-user/$', views.IndexUserSortReverseView.as_view(), name='indexusr'),
    url(r'^sort=subdivision/$', views.IndexSubdivisionSortView.as_view(), name='indexss'),
    url(r'^ruser/create/$', views.ruser_add, name='ruser_add'),
    url(r'^ruser/(?P<pk>\d+)/delete/$', views.RUserDelete.as_view(), name='ruser_delete'),
    url(r'^ruser/(?P<pk>\d+)/deleteinfo/$', views.RUserInfoDelete.as_view(), name='ruserinfo_delete'),
    url(r'^ruser/(?P<pk>\d+)/edit/$', views.ruser_edit, name='ruser-edit'),
    url(r'^subdivision/create/$', views.SubdivisionCreate.as_view(), name='subdivision_create'),
]