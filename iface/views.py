# -*- coding: utf-8 -*-  
from django.contrib.auth.decorators import permission_required

from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
import datetime

from .forms import RUserForm, RUserInfoForm
from django.shortcuts import render
from django.views import generic
from .models import RUser, RUserInfo, Subdivision
# Create your views here.
from subprocess import Popen, PIPE, call

class MainIndexView(generic.ListView):

    model = RUserInfo
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        # В первую очередь получаем базовую реализацию контекста
        context = super(MainIndexView, self).get_context_data(**kwargs)
        # Добавляем новую переменную к контексту и иниуиализируем ее некоторым значением
        p = Popen('freeradius -v', shell=True, stdout=PIPE)
        out, err = p.communicate()
        context['radv'] = bytes.decode(out).split('\n')

        p = Popen('service freeradius status', shell=True, stdout=PIPE)
        out, err = p.communicate()
        context['radservice'] = bytes.decode(out).split('\n')

        p = Popen('echo "Message-Authenticator = 0x00, FreeRADIUS-Statistics-Type = 1, Response-Packet-Type = Access-Accept" | radclient -x localhost:18121 status adminsecret', shell=True, stdout=PIPE)
        out, err = p.communicate()
        context['radstat1'] = ('FreeRADIUS-Total-Access-Requests' + bytes.decode(out).split('FreeRADIUS-Total-Access-Requests')[1]).split('\n')

        p = Popen('echo "Message-Authenticator = 0x00, FreeRADIUS-Statistics-Type = 2, Response-Packet-Type = Access-Accept" | radclient -x localhost:18121 status adminsecret', shell=True, stdout=PIPE)
        out, err = p.communicate()
        context['radstat2'] = ('FreeRADIUS-Total-Accounting-Requests' + bytes.decode(out).split('FreeRADIUS-Total-Accounting-Requests')[1]).split('\n')

        p = Popen('echo "Message-Authenticator = 0x00, FreeRADIUS-Statistics-Type = 4, Response-Packet-Type = Access-Accept" | radclient -x localhost:18121 status adminsecret', shell=True, stdout=PIPE)
        out, err = p.communicate()
        context['radstat4'] = ('FreeRADIUS-Total-Proxy-Access-Requests' + bytes.decode(out).split('FreeRADIUS-Total-Proxy-Access-Requests')[1]).split('\n')
        return context

class IndexView(generic.ListView):

    model = RUserInfo
    template_name = 'user_list.html'

class IndexNameSortView(generic.ListView):

    model = RUserInfo
    queryset = RUserInfo.objects.order_by("name", "surname")
    template_name = 'user_list.html'

class IndexNameSortReverseView(generic.ListView):

    model = RUserInfo
    queryset = RUserInfo.objects.order_by("-name", "-surname")
    template_name = 'user_list.html'

class IndexUserSortView(generic.ListView):

    model = RUserInfo
    queryset = RUserInfo.objects.order_by("user")
    template_name = 'user_list.html'

class IndexUserSortReverseView(generic.ListView):

    model = RUserInfo
    queryset = RUserInfo.objects.order_by("-user")
    template_name = 'user_list.html'


from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy


class RUserDelete(DeleteView):
    model = RUser
    success_url = reverse_lazy('user_list')

class RUserInfoDelete(DeleteView):
    model = RUserInfo
    success_url = reverse_lazy('user_list')


@permission_required('iface.can_see_users')
def ruser_add(request):

    if request.method == 'POST':

        # Создание экземпляра формы и заполнение их данными из запроса
        uform = RUserForm(data = request.POST)
        iform = RUserInfoForm(data = request.POST)

        # Проверка заполнения формы:
        if uform.is_valid() and iform.is_valid():
            #Сохранения данных из формы 
            ruser = uform.save()
            info = iform.save(commit = False)
            info.user = ruser
            
            info.save()


            # Перенаправление
            return HttpResponseRedirect(reverse('user_list') )

    # Если это GET запрос или какой другой, создается обычная форма.
    else:
        uform = RUserForm()
        iform = RUserInfoForm()

    return render(request, 'iface/ruser_add.html', {'uform': uform, 'iform': iform,})

def ruser_edit(request, pk):

    if request.method == 'POST':

        # Создание экземпляра формы и заполнение их данными из запроса, захват существующих данных
        uinst = RUser.objects.get(pk=pk)
        uform = RUserForm(request.POST, instance = uinst)
        iinst = RUserInfo.objects.get(user=uinst)
        iform = RUserInfoForm(request.POST, instance = iinst)

        # Проверка формы
        if uform.is_valid() and iform.is_valid():
            ruser = uform.save()
            info = iform.save(commit = False)
            info.user = ruser
            info.save()


            # Перенаправление:
            return HttpResponseRedirect(reverse('user_list') )
    else:
        # Заполнение формы новыми данными
        uinst = RUser.objects.get(pk=pk)

        uform = RUserForm(initial= {'username': uinst.username, 'password': uinst.password})
        iinst = RUserInfo.objects.get(user=uinst)
        iform = RUserInfoForm(initial= {'name': iinst.name, 'surname': iinst.surname, 'patronymic': iinst.patronymic, 'end_of_access': iinst.end_of_access})

    return render(request, 'iface/ruser_edit.html', {'uform': uform, 'iform': iform,})
